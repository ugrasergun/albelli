﻿using Albelli.API.Data;
using Albelli.API.Models;
using Albelli.API.Models.Enums;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Xunit;

namespace Albelli.Test.IntegrationTests
{
    public class OrderControllerTests : IClassFixture<WebApiTesterFactory<TestStartUp>>
    {
        private readonly WebApiTesterFactory<TestStartUp> _factory;
        private const string GetOrder = "api/Order/GetOrder?orderID={0}";
        private const string SubmitOrder = "/api/Order/SubmitOrder";


        public OrderControllerTests(WebApiTesterFactory<TestStartUp> factory)
        {
            _factory = factory;
        }



        [Theory]
        [InlineData(-1, "OrderID must be greater than 0 (Parameter 'OrderID')")]
        [InlineData(0, "OrderID must be greater than 0 (Parameter 'OrderID')")]
        [InlineData(2, "OrderID: 2 is not found. (Parameter 'OrderID')")]
        public async Task GetOrderShouldReturnBadRequest(int id, string expectedContent)
        {
            var response = await GetOrderAsync(id);

            response?.StatusCode.Should().Be(400);

            var actualContent = await response.Content.ReadAsStringAsync();

            Assert.Equal(expectedContent, actualContent);
        }

        [Fact]
        public async Task GetOrderShouldReturnExistingOrder()
        {
            var response = await GetOrderAsync(1);
            Action act = () => response.EnsureSuccessStatusCode();

            act.Should().NotThrow();  // Status Code 200-299

            var jsonResponse = await response.Content.ReadAsStringAsync();
            var orderResponse = JsonConvert.DeserializeObject<OrderResponse>(jsonResponse);

            Assert.Equal(2, orderResponse.OrderItems.Count);
            Assert.Equal(36, orderResponse.RequiredBinWidth);
        }

        [Fact]
        public async Task SubmitOrderShouldSaveOrderCorrectly()
        {
            var orderRequest = new OrderRequest { OrderID = 3, OrderItems = new List<OrderItem>() };
            orderRequest.OrderItems.Add(new OrderItem { ProductType = ProductType.photoBook, Quantity = 2 });
            orderRequest.OrderItems.Add(new OrderItem { ProductType = ProductType.calendar, Quantity = 1 });


            var response = await SubmitOrderAsync(orderRequest);

            Action act = () => response.EnsureSuccessStatusCode();

            act.Should().NotThrow();  // Status Code 200-299

            response = await GetOrderAsync(3);

            act = () => response.EnsureSuccessStatusCode();

            act.Should().NotThrow();  // Status Code 200-299

            var jsonResponse = await response.Content.ReadAsStringAsync();
            var orderResponse = JsonConvert.DeserializeObject<OrderResponse>(jsonResponse);

            Assert.Equal(orderRequest.OrderID, orderResponse.OrderID);
            Assert.Equal(orderRequest.OrderItems.Count, orderResponse.OrderItems.Count);
            Assert.Equal(48, orderResponse.RequiredBinWidth);
        }

        [Theory]
        [InlineData(-1, "OrderID must be greater than 0 (Parameter 'OrderID')")]
        [InlineData(0,  "OrderID must be greater than 0 (Parameter 'OrderID')")]
        [InlineData(1, "OrderID: 1 already exists (Parameter 'OrderID')")]
        public async Task SubmitOrderShouldFailOnIncorrectOrderID(int orderID, string expectedContent)
        {
            var orderRequest = new OrderRequest { OrderID = orderID, OrderItems = new List<OrderItem>() };
            orderRequest.OrderItems.Add(new OrderItem { ProductType = ProductType.photoBook, Quantity = 2 });

            var response = await SubmitOrderAsync(orderRequest);

            response?.StatusCode.Should().Be(400);

            var actualContent = await response.Content.ReadAsStringAsync();

            Assert.Equal(expectedContent, actualContent);
        }

        [Fact]
        public async Task SubmitOrderShouldFailOnIncorrectQuantity()
        {
            var orderRequest = new OrderRequest { OrderID = 4, OrderItems = new List<OrderItem>() };
            orderRequest.OrderItems.Add(new OrderItem { ProductType = ProductType.photoBook, Quantity = 0 });

            var response = await SubmitOrderAsync(orderRequest);

            response?.StatusCode.Should().Be(400);

            var actualContent = await response.Content.ReadAsStringAsync();
            Assert.Equal("Quantity must be greater than 0 (Parameter 'Quantity')", actualContent);
        }

        [Fact]
        public async Task SubmitOrderShouldFailOnEmptyOrderItems()
        {
            var orderRequest = new OrderRequest { OrderID = 5, OrderItems = new List<OrderItem>() };

            var response = await SubmitOrderAsync(orderRequest);

            response?.StatusCode.Should().Be(400);

            var actualContent = await response.Content.ReadAsStringAsync();
            Assert.Equal("Order Must have at least one item (Parameter 'OrderItems')", actualContent);
        }

        [Fact]
        public async Task SubmitOrderShouldFailOnNullOrderItems()
        {
            var orderRequest = new OrderRequest { OrderID = 6 };

            var response = await SubmitOrderAsync(orderRequest);

            response?.StatusCode.Should().Be(400);

            var actualContent = await response.Content.ReadAsStringAsync();
            Assert.Equal("Order Must have at least one item (Parameter 'OrderItems')", actualContent);
        }

        [Theory]
        [InlineData(100, 1, 1)]
        [InlineData(101, 3, 1)]
        [InlineData(102, 4, 1)]
        [InlineData(103, 5, 2)]
        [InlineData(104, 6, 2)]
        [InlineData(105, 8, 2)]
        [InlineData(106, 9, 3)]
        [InlineData(107, 100, 25)]
        public async Task SubmitOrderShouldStackMugsCorrectly(int orderID, int quantity, int expectedStackCount)
        {
            var orderRequest = new OrderRequest { OrderID = orderID, OrderItems = new List<OrderItem>() };
            orderRequest.OrderItems.Add(new OrderItem { ProductType = ProductType.mug, Quantity = quantity });

            var response = await SubmitOrderAsync(orderRequest);

            Action act = () => response.EnsureSuccessStatusCode();

            act.Should().NotThrow();  // Status Code 200-299

            response = await GetOrderAsync(orderID);

            act = () => response.EnsureSuccessStatusCode();

            act.Should().NotThrow();  // Status Code 200-299

            var jsonResponse = await response.Content.ReadAsStringAsync();
            var orderResponse = JsonConvert.DeserializeObject<OrderResponse>(jsonResponse);

            var expectedWidth = expectedStackCount * 94m;

            Assert.Equal(expectedWidth, orderResponse.RequiredBinWidth);
        }


        private async Task<HttpResponseMessage> GetOrderAsync(int id)
        {
            var httpClient = _factory.CreateClient();

            var uri = string.Format(GetOrder, id);

            HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Get, uri);

            return await httpClient.SendAsync(message);
        }

        private async Task<HttpResponseMessage> SubmitOrderAsync(OrderRequest request)
        {
            var settings = new JsonSerializerSettings
            {
                Converters = { new StringEnumConverter() },
            };

            var jsonData = JsonConvert.SerializeObject(request, settings);

            var httpClient = _factory.CreateClient();

            HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Post, SubmitOrder);

            var requestContent = new StringContent(jsonData);
            requestContent.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");

            message.Content = requestContent;

            return await httpClient.SendAsync(message);

        }
    }
}
