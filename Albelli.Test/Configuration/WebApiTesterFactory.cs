﻿using Albelli.API.Data;
using Albelli.API.Models;
using Albelli.API.Models.Enums;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albelli.Test
{
	public class WebApiTesterFactory<TestStartUp> : WebApplicationFactory<TestStartUp> where TestStartUp : class
	{
		private OrderContext _context;

		public WebApiTesterFactory()
        {
			var options = new DbContextOptionsBuilder<OrderContext>();
			options.UseSqlite("DataSource=Test.db");

			_context = new OrderContext(options.Options);
			_context.Database.EnsureDeleted();
			_context.Database.Migrate();

			var order = new Order { OrderID = 1, ProductOrders = new List<ProductOrder>() };
			order.ProductOrders.Add(new ProductOrder { ProductType = ProductType.calendar, Quantity = 2 });
			order.ProductOrders.Add(new ProductOrder { ProductType = ProductType.canvas, Quantity = 1 });
			_context.Orders.Add(order);
			_context.SaveChanges();
		}

		protected override IWebHostBuilder CreateWebHostBuilder()
		{
			return WebHost.CreateDefaultBuilder()
				.UseStartup<TestStartUp>();
		}

		protected override void ConfigureWebHost(IWebHostBuilder builder)
		{
			builder.UseContentRoot(".");
			base.ConfigureWebHost(builder);
		}
	}
}
