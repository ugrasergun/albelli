# Albelli.API

Albelli.Api is a service that would store orders in SQL Server and retrieves saved orders with information about the minimum width for a bin to include all items in the order. 

### Prerequisites 

You must have docker set up in your system 

## Installation

On the project folder (Albelli.API) run this command
```
docker-compose up
```

## Usage

When Docker container is up API will be accessible from http://localhost:8000/index.html.
DB will be accessible by any client with following Connection String 
```
"Server=localhost,1433;Database=Albelli;User=sa;Password=Example123;"
```
