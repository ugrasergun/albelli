﻿using Albelli.API.Models;
using Albelli.API.Models.Enums;
using Microsoft.EntityFrameworkCore;

namespace Albelli.API.Data
{
    public class OrderContext : DbContext
    {

        public OrderContext(DbContextOptions<OrderContext> options) : base(options)
        {
        }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductOrder> ProductOrders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>().ToTable("Order");
            modelBuilder.Entity<Order>().Property(o => o.OrderID).ValueGeneratedNever();
            modelBuilder.Entity<Product>().ToTable("Product").HasData(
                new Product { ProductType = ProductType.photoBook, MaxStackCount = 1, Width = 19 },
                new Product { ProductType = ProductType.calendar, MaxStackCount = 1, Width = 10 },
                new Product { ProductType = ProductType.canvas, MaxStackCount = 1, Width = 16 },
                new Product { ProductType = ProductType.cards, MaxStackCount = 1, Width = 4.7m },
                new Product { ProductType = ProductType.mug, MaxStackCount = 4, Width = 94 }
                );
            modelBuilder.Entity<ProductOrder>(po =>
            {
                po.HasKey(p => new { p.ProductType, p.OrderId });
                po.ToTable("ProductOrder");
            });


        }
    }
}
