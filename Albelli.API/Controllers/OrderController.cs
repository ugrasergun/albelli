﻿using Albelli.API.Models;
using Albelli.API.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Albelli.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost("SubmitOrder")]
        public async Task<IActionResult> SubmitOrder(OrderRequest orderRequest)
        {
            try
            {
                await _orderService.SubmitOrder(orderRequest);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpGet("GetOrder")]
        public async Task<ActionResult<OrderResponse>> GetOrder(int orderID)
        {
            try
            {
                return await _orderService.GetOrder(orderID);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
