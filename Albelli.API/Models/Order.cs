﻿using System.Collections.Generic;

namespace Albelli.API.Models
{
    public class Order
    {
        public int OrderID { get; set; }
        public ICollection<ProductOrder> ProductOrders { get; set; }
    }
}