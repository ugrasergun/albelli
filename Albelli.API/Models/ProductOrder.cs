﻿using Albelli.API.Models.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace Albelli.API.Models
{
    public class ProductOrder
    {
        [ForeignKey("Product")]
        public ProductType ProductType { get; set; }
        public int OrderId { get; set; }
        public int Quantity { get; set; }
        public Product Product { get; set; }
        public Order Order { get; set; }
    }
}
