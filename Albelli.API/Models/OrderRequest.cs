﻿using System.Collections.Generic;

namespace Albelli.API.Models
{
    public class OrderRequest
    {
        public int OrderID { get; set; }
        public ICollection<OrderItem> OrderItems { get; set; }

    }
}
