﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albelli.API.Models
{
    public class OrderResponse
    {
        public int OrderID { get; set; }
        public ICollection<OrderItem> OrderItems { get; set; }
        public decimal RequiredBinWidth { get; set; }
    }
}
