﻿using Albelli.API.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albelli.API.Models
{
    public class OrderItem
    {
        public ProductType ProductType { get; set; }
        public int Quantity { get; set; }
    }
}
