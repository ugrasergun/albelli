﻿using Albelli.API.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace Albelli.API.Models
{
    public class Product
    {
        [Key]
        public ProductType ProductType { get; set; }
        public decimal Width { get; set; }
        public int MaxStackCount { get; set; }
    }
}
