﻿using Albelli.API.Data;
using Albelli.API.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albelli.API.Services
{
    public interface IOrderService
    {
        Task SubmitOrder(OrderRequest request);
        Task<OrderResponse> GetOrder(int orderId);
    }
    public class OrderService : IOrderService
    {
        private readonly OrderContext _orderContext;

        public OrderService(OrderContext orderContext)
        {
            _orderContext = orderContext;
        }

        public async Task SubmitOrder(OrderRequest request)
        {
            if(request.OrderID <= 0)
            {
                throw new ArgumentOutOfRangeException("OrderID", "OrderID must be greater than 0");
            }

            if(request.OrderItems == null || request.OrderItems.Count == 0)
            {
                throw new ArgumentException("Order Must have at least one item", "OrderItems");
            }

            if (request.OrderItems.Any(oi => oi.Quantity <= 0))
            {
                throw new ArgumentOutOfRangeException("Quantity" ,"Quantity must be greater than 0");
            }


            if(await _orderContext.Orders.AnyAsync(o => o.OrderID == request.OrderID))
            {
                throw new ArgumentException($"OrderID: {request.OrderID} already exists", "OrderID");
            }

            var order = new Order { 
                OrderID = request.OrderID, 
                ProductOrders = new List<ProductOrder>() 
            };

            foreach (var orderItem in request.OrderItems)
            {
                order.ProductOrders.Add(new ProductOrder { OrderId = request.OrderID, ProductType = orderItem.ProductType, Quantity = orderItem.Quantity });
            }

           await _orderContext.AddAsync<Order>(order);
           await _orderContext.SaveChangesAsync();
        }

        public async Task<OrderResponse> GetOrder(int orderId)
        {
            if (orderId <= 0)
            {
                throw new ArgumentOutOfRangeException("OrderID", "OrderID must be greater than 0");
            }
            var order = await _orderContext.Orders
                .Where(o => o.OrderID == orderId)
                .Include(o=> o.ProductOrders)
                .ThenInclude(po => po.Product)
                .FirstOrDefaultAsync();
            if(order == null)
            {
                throw new ArgumentException($"OrderID: { orderId } is not found.", "OrderID");
            }

            var orderResponse = new OrderResponse { OrderID = orderId, OrderItems = new List<OrderItem>() };
            decimal requiredBinWidth = 0;

            foreach (var item in order.ProductOrders)
            {
                orderResponse.OrderItems.Add(new OrderItem { ProductType = item.ProductType, Quantity = item.Quantity });
                requiredBinWidth += CalculateBinWidth(item);
            }
            orderResponse.RequiredBinWidth = requiredBinWidth;
            return orderResponse;
        }

        private decimal CalculateBinWidth(ProductOrder po)
        {
            //Formula for rounding up a Integer Division
            var stackSize = (po.Quantity + po.Product.MaxStackCount - 1) / po.Product.MaxStackCount;

            return stackSize * po.Product.Width;
        }
    }
}
